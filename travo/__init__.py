__version__ = "0.2.2"

from .gitlab import Forge, GitLab
from .assignment import Assignment
from .course import Course
from .homework import Homework
