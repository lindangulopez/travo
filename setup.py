# -*- encoding: utf-8 -*-
from setuptools import setup     # type: ignore

setup(
    name="travo",
    description='Management tools for gitlab-based assignment workflows',
    url='https://gitlab.com/travo-cr/travo.git/',
    author='Pierre Thomas Froidevaux, Alexandre Blondin-Massé, Jean Privat, and Nicolas M. Thiéry',
    author_email='Nicolas.Thiery@universite-paris-saclay.fr',
    license='CC',
    classifiers=[
      'Development Status :: 4 - Alpha',
      'Intended Audience :: Information Technology'
      'Topic :: Scientific/Engineering',
      'Programming Language :: Python :: 3',
    ],  # classifiers list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
    packages=['travo'],
    package_data={'travo': ['py.typed', 'locale/travo.*.yml']},
    install_requires=[
        'colorlog',
        'dataclasses',    # For Python < 3.7
        'deprecation',
        'typing_utils',   # For Python < 3.8 and issubtype
        'requests',
        'setuptools>=6.0',  # For Python < 3.7
        'tqdm',
        'anybadge',
        'python-i18n[YAML]',
    ],
    entry_points={
        "console_scripts": [
            "travo = travo.console_scripts:travo",
            "travo-echo-travo-token = travo.console_scripts:travo_echo_travo_token",
        ],
    },
)
